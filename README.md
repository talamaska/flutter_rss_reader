# Flutter RSS Reader


## Getting Started

### This project is a RSS/Atom feed reader.

- You can add feeds
- Display all items sorted by date (pubDate or dc:date). More recent first.
- Add keywords groups, comma separated, to filter feed items. Displayed are only those that match all keywords in a group.
- View filtered news for each group of keywords
- Delete Feeds
- Delete group of keywords

### A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.io/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.io/docs/cookbook)


## Resources used to build this app
- [State management](https://medium.com/@agungsurya/basic-state-management-in-google-flutter-6ee73608f96d)
- [Stream](https://medium.com/flutterpub/exploring-streams-in-flutter-4732e5524dd8)
- [BloC pattern 1](https://medium.com/flutterpub/architecting-your-flutter-project-bd04e144a8f1)
- [BloC pattern 2](https://medium.com/flutterpub/architect-your-flutter-project-using-bloc-pattern-part-2-d8dd1eca9ba5)
- [BloC Provider](https://www.didierboelens.com/2018/08/reactive-programming---streams---bloc/)
- [SQlite for Flutter](https://applandeo.com/blog/sqflite-flutter-database/)

## 3d party packages used in the app
- [webfeed](https://pub.dartlang.org/packages/webfeed)
- [validators](https://pub.dartlang.org/packages/validators)
- [rxdart](https://pub.dartlang.org/packages/rxdart)
- [sqflite](https://pub.dartlang.org/packages/sqflite)
- [path](https://pub.dartlang.org/packages/path)
- [intl](https://pub.dartlang.org/packages/intl)
- [url_launcher](https://pub.dartlang.org/packages/url_launcher)
- [cached_network_image](https://pub.dartlang.org/packages/cached_network_image)
- [xml2json](https://pub.dartlang.org/packages/xml2json)
- [font_awesome_flutter](https://pub.dartlang.org/packages/font_awesome_flutter)
- [flutter_slidable](https://pub.dartlang.org/packages/flutter_slidable)


For help getting started with Flutter, view our 
[online documentation](https://flutter.io/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.
