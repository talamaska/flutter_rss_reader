import 'dart:async';

import 'package:flutter_rss_reader/models/feed_item_model.dart';
import 'package:flutter_rss_reader/models/feed_model.dart';
import 'package:flutter_rss_reader/providers/feed_provider.dart';



class FeedRepository {
  final FeedProvider feedProvider = FeedProvider();

  Future<FeedModel> getFeed(String url) => feedProvider.getFeed(url);
  Future<List<FeedItemModel>> getFeedItems(String url) => feedProvider.getFeedItems(url);
  Future<String> getItemImage(String url) => feedProvider.getItemImage(url);
}