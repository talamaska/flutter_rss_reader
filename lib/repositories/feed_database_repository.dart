import 'dart:async';

import 'package:flutter_rss_reader/providers/database_provider.dart';
import 'package:flutter_rss_reader/daos/feed_dao.dart';
import 'package:flutter_rss_reader/models/feed_model.dart';
import 'package:flutter_rss_reader/models/database_repository.dart';

import 'package:sqflite/sqflite.dart';

class FeedsDatabaseRepository implements DatabaseRepository<FeedModel> {
  FeedsDatabaseRepository(this.databaseProvider);

  final FeedDao dao = FeedDao();

  @override
  DatabaseProvider databaseProvider;


  @override
  Future<FeedModel> insert(FeedModel feed) async {
    final Database db = await databaseProvider.db();
    final int id = await db.insert(dao.tableName, dao.toMap(feed));
    return FeedModel(id: id, url: feed.url, type: feed.type);
  }

  @override
  Future<FeedModel> delete(FeedModel feed) async {
    final Database db = await databaseProvider.db();
    await db.delete(dao.tableName,
        where: dao.columnId + ' = ?', whereArgs: <int>[feed.id]);
    return feed;
  }

  @override
  Future<FeedModel> update(FeedModel feed) async {
    final Database db = await databaseProvider.db();
    await db.update(dao.tableName, dao.toMap(feed),
        where: dao.columnId + ' = ?', whereArgs: <int>[feed.id]);
    return feed;
  }

  @override
  Future<List<FeedModel>> getAllItems() async {
    final Database db = await databaseProvider.db();
    final List<Map<String, dynamic>> maps = await db.query(dao.tableName);
    return dao.fromList(maps);
  }
}