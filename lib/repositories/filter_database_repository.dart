import 'dart:async';

import 'package:flutter_rss_reader/providers/database_provider.dart';
import 'package:flutter_rss_reader/daos/filter_dao.dart';
import 'package:flutter_rss_reader/models/filter_model.dart';
import 'package:flutter_rss_reader/models/database_repository.dart';
import 'package:sqflite/sqflite.dart';

class FilterDatabaseRepository implements DatabaseRepository<FilterModel> {
  FilterDatabaseRepository(this.databaseProvider);

  final FilterDao dao = FilterDao();

  @override
  DatabaseProvider databaseProvider;


  @override
  Future<FilterModel> insert(FilterModel filter) async {
    final Database db = await databaseProvider.db();
    final int id = await db.insert(dao.tableName, dao.toMap(filter));
    return FilterModel(id: id, keywords: filter.keywords);
  }

  @override
  Future<FilterModel> delete(FilterModel filter) async {
    final Database db = await databaseProvider.db();
    await db.delete(dao.tableName,
        where: dao.columnId + ' = ?', whereArgs: <int>[filter.id]);
    return filter;
  }

  @override
  Future<FilterModel> update(FilterModel filter) async {
    final Database db = await databaseProvider.db();
    await db.update(dao.tableName, dao.toMap(filter),
        where: dao.columnId + ' = ?', whereArgs: <int>[filter.id]);
    return filter;
  }

  @override
  Future<List<FilterModel>> getAllItems() async {
    final Database db = await databaseProvider.db();
    final List<Map<String, dynamic>> maps = await db.query(dao.tableName);
    return dao.fromList(maps);
  }
}