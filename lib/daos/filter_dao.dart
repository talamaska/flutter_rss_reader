import 'package:flutter_rss_reader/models/dao.dart';
import 'package:flutter_rss_reader/models/filter_model.dart';


class FilterDao implements Dao<FilterModel> {
  final String tableName = 'filters';
  final String columnId = 'id';
  final String _columnKeywords = 'keywords';


  @override
  String get createTableQuery =>
      'CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY,'
          ' $_columnKeywords TEXT)';

  @override
  FilterModel fromMap(Map<String, dynamic> query) {
    final FilterModel filter = FilterModel(
      id: query[columnId],
      keywords: query[_columnKeywords]
    );
    return filter;
  }

  @override
  Map<String, dynamic> toMap(FilterModel object) {
    return <String, dynamic>{
      _columnKeywords: object.keywords,
    };
  }
  @override
  List<FilterModel> fromList(List<Map<String,dynamic>> query) {
    final List<FilterModel> filters = <FilterModel>[];
    for (Map<String,dynamic> map in query) {
      filters.add(fromMap(map));
    }
    return filters;
  }
}