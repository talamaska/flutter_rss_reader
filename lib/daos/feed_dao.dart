import 'package:flutter_rss_reader/models/dao.dart';
import 'package:flutter_rss_reader/models/feed_model.dart';

class FeedDao implements Dao<FeedModel> {
  final String tableName = 'feeds';
  final String columnId = 'id';
  final String _columnTitle = 'title';
  final String _columnUrl = 'url';
  final String _columnType = 'type';


  @override
  String get createTableQuery =>
      'CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY,'
          ' $_columnTitle TEXT,'
          ' $_columnType TEXT,'
          ' $_columnUrl TEXT)';

  @override
  FeedModel fromMap(Map<String, dynamic> query) {
    final FeedModel feed = FeedModel(
      id: query[columnId],
      title: query[_columnTitle],
      url: query[_columnUrl],
      type: query[_columnType]
    );

    return feed;
  }

  @override
  Map<String, dynamic> toMap(FeedModel object) {
    return <String, dynamic>{
      _columnTitle: object.title,
      _columnUrl: object.url,
      _columnType: object.type,
    };
  }
  @override
  List<FeedModel> fromList(List<Map<String,dynamic>> query) {
    final List<FeedModel> feeds = <FeedModel>[];
    for (Map<String,dynamic> map in query) {
      feeds.add(fromMap(map));
    }
    return feeds;
  }
}