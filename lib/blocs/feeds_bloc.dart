import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:flutter_rss_reader/models/feed_item_model.dart';
import 'package:flutter_rss_reader/models/filter_model.dart';
import 'package:flutter_rss_reader/repositories/filter_database_repository.dart';
import 'package:flutter_rss_reader/providers/bloc_provider.dart';
import 'package:flutter_rss_reader/providers/database_provider.dart';
import 'package:flutter_rss_reader/repositories/feed_database_repository.dart';
import 'package:flutter_rss_reader/models/feed_model.dart';

import 'package:flutter_rss_reader/repositories/feed_repository.dart';


class FeedsBloc extends BlocBase {
  FeedsBloc(this.dbProvider) {
    feedRepo = FeedRepository();
    feedsRepo = FeedsDatabaseRepository(dbProvider);
    filterRepo = FilterDatabaseRepository(dbProvider);

    feedStream.listen(_addFeed);
    filterStream.listen(_addFilter);
  }

  final BehaviorSubject<FeedModel> _feedSubject = BehaviorSubject<FeedModel>(seedValue: null);
  final PublishSubject<List<FeedModel>> _feedsSubject = PublishSubject<List<FeedModel>>();

  final BehaviorSubject<FilterModel> _filterSubject = BehaviorSubject<FilterModel>(seedValue: null);
  final PublishSubject<List<FilterModel>> _filtersSubject = PublishSubject<List<FilterModel>>();

  final PublishSubject<String> _errors = PublishSubject<String>();
  final ReplaySubject<List<FeedItemModel>> _feedsItems = ReplaySubject<List<FeedItemModel>>(maxSize: 1);

  final PublishSubject<List<FeedItemModel>> _feedItems = PublishSubject<List<FeedItemModel>>();

  final ReplaySubject<List<FeedItemModel>> _filteredItems = ReplaySubject<List<FeedItemModel>>(maxSize: 1);


  final DatabaseProvider dbProvider;
  FeedRepository feedRepo;
  FeedsDatabaseRepository feedsRepo;
  FilterDatabaseRepository filterRepo;

  List<FeedItemModel> itemList = <FeedItemModel>[];
  List<FeedModel> feedList = <FeedModel>[];

  int _lastUpdate = 0;
  //  Map<String, int> _lastUpdateFeed = {};
  //  Map<String, FeedItemModel> _itemsPerFeed = {};
  final int _updatePeriod = 1000 * 60 * 15;
  StreamSubscription<List<FeedItemModel>> itemsSub;

  StreamSink<FeedModel> get feed => _feedSubject.sink;
  Observable<FeedModel> get feedStream => _feedSubject.stream;

  StreamSink<List<FeedModel>> get _feedsIn => _feedsSubject.sink;
  Observable<List<FeedModel>> get feeds => _feedsSubject.stream;

  StreamSink<FilterModel> get filter => _filterSubject.sink;
  Observable<FilterModel> get filterStream => _filterSubject.stream;

  StreamSink<List<FeedItemModel>> get _feedsItemsIn => _feedsItems.sink;
  Observable<List<FeedItemModel>> get feedsItems => _feedsItems.stream;

  StreamSink<List<FeedItemModel>> get _feedItemsIn => _feedItems.sink;
  Observable<List<FeedItemModel>> get feedItems => _feedItems.stream;

  StreamSink<List<FilterModel>> get _filtersIn => _filtersSubject.sink;
  Observable<List<FilterModel>> get filters => _filtersSubject.stream;

  StreamSink<List<FeedItemModel>> get _filteredItemsIn => _filteredItems.sink;
  Observable<List<FeedItemModel>> get filteredItems => _filteredItems.stream;

  StreamSink<String> get errorsIn => _errors.sink;
  Observable<String> get errors => _errors.stream;

  Future<void> _addFeed(FeedModel f) async {
    debugPrint('_addFeed $f');
    try {
      final FeedModel _f = await feedRepo.getFeed(f.url);
      feedsRepo.insert(_f).then((FeedModel _inserted) {
        debugPrint('inserted $_inserted');

        getFeeds();
      });
    } catch(e) {
      debugPrint('err1 $e');
      errorsIn.add(e.toString());
    }
  }

  Future<void> _addFilter(FilterModel f) async {
    debugPrint('_addFeed $f');
    try {
      filterRepo.insert(f).then((FilterModel _inserted) {
        debugPrint('inserted $_inserted');

        getFilters();
      });

    } catch(e) {
      debugPrint('err1 $e');
      errorsIn.add(e.toString());
    }
  }


  Future<void> getFeeds() async {
    try {
      final List<FeedModel> _feedList = await feedsRepo.getAllItems();
      feedList = _feedList;
      _feedsIn.add(_feedList);

      debugPrint('got Feeds');
    } catch(e) {
      debugPrint('err2 $e');
      _feedsIn.addError(e);
    }
  }

  Future<void> getFilters() async {
    try {
      final List<FilterModel> filters = await filterRepo.getAllItems();
      _filtersIn.add(filters);

      debugPrint('got Filters');
    } catch(e) {
      debugPrint('err2 $e');
      _filtersIn.addError(e);
    }
  }

  Future<Null> getFeed(String url) async {
    final Completer<Null> completer = Completer<Null>();
//    final int _currentTime = DateTime.now().millisecondsSinceEpoch;

    List<FeedItemModel> list = await feedRepo.getFeedItems(url);
//    _lastUpdateFeed[url] = DateTime.now().millisecondsSinceEpoch;

    list = list.map((FeedItemModel item) {
      DateTime dateTime;
      if(item.date != null) {
        try {
          dateTime = DateTime.parse(item.date);
        } catch(ex) {
          final DateFormat dateFormat = DateFormat();
          dateFormat.addPattern('EEE, dd MMM yyyy HH:mm:ss z');
          dateTime = dateFormat.parse(item.date);
        }
      }

      return FeedItemModel(
          title: item.title,
          description: item.description,
          image: item.image,
          date: item.date,
          link: item.link,
          dateObj: dateTime,
          dateMiliseconds: dateTime.millisecondsSinceEpoch
      );
    }).toList();

    _feedItemsIn.add(list);
    completer.complete();

    return completer.future;
  }

  Future<String> getItemImage(FeedItemModel item) async {
    String image = item.image;
    if(item.link != '' && item.image == null) {
      image = await feedRepo.getItemImage(item.link);
    }
    return image;
  }

  Future<Null> getFeedItems() async {
    final Completer<Null> completer = Completer<Null>();
    final int _currentTime = DateTime.now().millisecondsSinceEpoch;
    final List<FeedModel> _feedList = await feedsRepo.getAllItems();

    if(_currentTime - _lastUpdate > _updatePeriod || itemList.isEmpty || feedList.length != _feedList.length) {
      debugPrint('items empty or time to update 1');
      try {
        debugPrint('got feeds');
        if(_feedList.isNotEmpty) {
          final List<Future<List<FeedItemModel>>> futures = <Future<List<FeedItemModel>>>[];

          for(int i=0; i<_feedList.length; i++) {
            futures.add(feedRepo.getFeedItems(_feedList[i].url).catchError(() => <FeedItemModel>[]));
          }

          final List<List<FeedItemModel>> responses = await Future.wait(futures);

          List<FeedItemModel> list = responses.expand<FeedItemModel>((dynamic x) => x).toList().cast<FeedItemModel>();
          debugPrint('got list 1');
          list = list.map((FeedItemModel item) {
            DateTime dateTime;
            if(item.date != null) {
              try {
                dateTime = DateTime.parse(item.date);
              } catch(ex) {
                final DateFormat dateFormat = DateFormat();
                dateFormat.addPattern('EEE, dd MMM yyyy HH:mm:ss z');
                dateTime = dateFormat.parse(item.date);
              }
            }


            return FeedItemModel(
                title: item.title,
                description: item.description,
                image: item.image,
                date: item.date,
                link: item.link,
                dateObj: dateTime,
                dateMiliseconds: dateTime.millisecondsSinceEpoch
            );
          }).toList();

          list.sort((FeedItemModel a, FeedItemModel b) {
            return b.dateMiliseconds - a.dateMiliseconds;
          });

          _feedsItems.add(list);
          itemList = list;
          _lastUpdate = DateTime.now().millisecondsSinceEpoch;

          completer.complete();
        } else {
          _feedsItems.add(<FeedItemModel>[]);
          completer.complete();
        }

      } catch (e) {
        _feedsItemsIn.addError(e);
        completer.completeError(e);
      }
    } else {
      _feedsItems.add(itemList);
      completer.complete();
    }

    return completer.future;
  }

  Future<Null> getFilteredItems(String keywords) async {
    final Completer<Null> completer = Completer<Null>();
    final int _currentTime = DateTime.now().millisecondsSinceEpoch;

    if(_currentTime - _lastUpdate > _updatePeriod || itemList.isEmpty) {
      if(itemsSub != null) {
        itemsSub.cancel();
      }
      debugPrint('items empty or time to update 2');
      await getFeedItems();
      itemsSub = feedsItems.listen((List<FeedItemModel> items) {
        debugPrint('got new items 2');
        _filterItems(items, keywords);

      });
    } else {
      debugPrint('has cached items');
      _filterItems(itemList, keywords);
    }

    completer.complete();

    return completer.future;
  }

  void _filterItems(List<FeedItemModel> items, String keywords) {
    final List<String> _keywords = keywords.split(',');
    final List<FeedItemModel> _filteredList = items.where((FeedItemModel feedItem) {
      int score = 0;
      for(int i = 0; i < _keywords.length; i++) {

        if(feedItem.title.toLowerCase().contains(_keywords[i])) {
          score += 1;
        } else if (feedItem.description != null && feedItem.description.toLowerCase().contains(_keywords[i])) {
          score += 1;
        }
      }
      return score == _keywords.length;
    }).toList();

    _filteredItemsIn.add(_filteredList);
  }


  Future<FeedModel> removeFeed(int id) async {
    final FeedModel _deleted = await feedsRepo.delete(FeedModel(id: id));
    getFeeds();
    return _deleted;
  }

  Future<FilterModel> removeFilter(int id) async {
    final FilterModel _deleted = await filterRepo.delete(FilterModel(id: id));
    getFilters();
    return _deleted;
  }


  @override
  Future<void> dispose() async {
    _feedSubject.close();
    _feedsSubject.close();

    _filterSubject.close();
    _filtersSubject.close();

    _errors.close();
    _feedsItems.close();
    itemList = <FeedItemModel>[];
  }
}

