import 'package:meta/meta.dart';

@immutable
class FeedModel {
  const FeedModel({
    this.id,
    this.title,
    this.url,
    this.type
  });

  final int id;
  final String title;
  final String url;
  final String type;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is FeedModel &&
              runtimeType == other.runtimeType &&
              id == other.id &&
              title == other.title &&
              url == other.url &&
              type == other.type;

  //New
  @override
  int get hashCode =>
      id.hashCode ^ title.hashCode ^ url.hashCode ^ type.hashCode;

  @override
  String toString() {
    return 'FeedModel(id: $id, title: $title, url: $url, type: $type)';
  }
}