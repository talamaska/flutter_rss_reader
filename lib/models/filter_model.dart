import 'package:meta/meta.dart';

@immutable
class FilterModel {
  const FilterModel({
    this.id,
    this.keywords,
  });

  final int id;
  final String keywords;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is FilterModel &&
              runtimeType == other.runtimeType &&
              id == other.id &&
              keywords == other.keywords;

  //New
  @override
  int get hashCode =>
      id.hashCode ^ keywords.hashCode;

  @override
  String toString() {
    return 'FilterModel(id: $id, keywords: $keywords)';
  }
}