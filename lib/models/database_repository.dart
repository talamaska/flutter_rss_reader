import 'dart:async';

import 'package:flutter_rss_reader/providers/database_provider.dart';


abstract class DatabaseRepository<T> {
  DatabaseProvider databaseProvider;
  Future<T> insert(T item);
  Future<T> update(T item);
  Future<T> delete(T item);
  Future<List<T>> getAllItems();
}