import 'package:meta/meta.dart';

@immutable
class FeedItemModel {
  const FeedItemModel(
      {this.title,
      this.image,
      this.date,
      this.dateObj,
      this.dateMiliseconds,
      this.link,
      this.description});

  final String title;
  final String image;
  final String date;
  final DateTime dateObj;
  final int dateMiliseconds;
  final String link;
  final String description;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FeedItemModel &&
          runtimeType == other.runtimeType &&
          title == other.title &&
          image == other.image &&
          date == other.date &&
          dateObj == other.dateObj &&
          dateMiliseconds == other.dateMiliseconds &&
          link == other.link &&
          description == other.description;

  //New
  @override
  int get hashCode =>
      title.hashCode ^
      image.hashCode ^
      date.hashCode ^
      dateObj.hashCode ^
      dateMiliseconds.hashCode ^
      link.hashCode ^
      description.hashCode;

  @override
  String toString() {
    return 'FeedItemModel(title: $title, image: $image, link: $link, dateMiliseconds: $dateMiliseconds)';
  }
}
