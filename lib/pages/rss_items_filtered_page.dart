import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/models/feed_item_model.dart';
import 'package:flutter_rss_reader/widgets/error_message.dart';

import 'package:flutter_rss_reader/widgets/rss_list_items.dart';

class RssItemsFilteredPage extends StatefulWidget {
  const RssItemsFilteredPage({
    this.bloc,
    this.keywords
  });

  final FeedsBloc bloc;
  final String keywords;

  @override
  _RssItemsFilteredPageState createState() => _RssItemsFilteredPageState();
}

class _RssItemsFilteredPageState extends State<RssItemsFilteredPage> {
  @override
  void initState() {
    super.initState();
    widget.bloc.getFilteredItems(widget.keywords);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Filtered News'),
        ),
        body: RefreshIndicator(
          onRefresh: () {
            return widget.bloc.getFilteredItems(widget.keywords);
          },
          color: Colors.orange,
          child: StreamBuilder<List<FeedItemModel>>(
              stream: widget.bloc.filteredItems,
              initialData: const <FeedItemModel>[],
              builder: (BuildContext context, AsyncSnapshot<List<FeedItemModel>> snapshot) {
                if (snapshot.hasError) {
                  return ErrorMsg(msg: 'Error: ${snapshot.error}');
                }
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  default:
                    if (snapshot.data.isEmpty) {
                      return Container(
                        child: const Text('No data'),
                        padding: const EdgeInsets.all(16.0),
                        alignment: Alignment.center,
                      );
                    }
                    return RssListItems(data: snapshot.data, bloc: widget.bloc);
                }
              }
          ),
        ),
    );
  }


}
