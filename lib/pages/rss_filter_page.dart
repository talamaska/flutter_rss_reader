import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/models/filter_model.dart';
import 'package:flutter_rss_reader/widgets/custom_progress_indicator.dart';
import 'package:flutter_rss_reader/widgets/error_message.dart';
import 'package:flutter_rss_reader/widgets/filter_list.dart';
import 'package:flutter_rss_reader/widgets/no_data.dart';
import 'package:flutter_rss_reader/widgets/rss_filter_dialog.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class RssFilterPage extends StatefulWidget {
  const RssFilterPage({this.bloc});

  final FeedsBloc bloc;

  @override
  State<StatefulWidget> createState() {
    return RssFilterPageState();
  }
}

class RssFilterPageState extends State<RssFilterPage> {
  SlidableController slidableController;

  @override
  void initState() {
    widget.bloc.getFilters();
    slidableController = SlidableController();
    super.initState();
  }

  void _showSnackBar(BuildContext context, String text) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(text),
      duration: Duration(milliseconds: 3000),
    ));
  }

  Future<void> _onDismiss(int index, int id, BuildContext context) async {
    try {
      await widget.bloc.removeFilter(id);
      _showSnackBar(context, 'Filter deleted');
    } catch (e) {
      _showSnackBar(context, 'Filter not deleted');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Filters'),
      ),
      body: StreamBuilder<List<FilterModel>>(
          stream: widget.bloc.filters,
          initialData: const <FilterModel>[],
          builder: (BuildContext context,
              AsyncSnapshot<List<FilterModel>> snapshot) {
            if (snapshot.hasError) {
              return ErrorMsg(msg: snapshot.error);
            }
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return CustomProgressIndicator();
              default:
                if (snapshot.data.isEmpty) {
                  return const NoData(msg: 'No filters were created');
                }
                return FilterList(
                    bloc: widget.bloc,
                    data: snapshot.data,
                    controller: slidableController,
                    onDismiss: _onDismiss,
                    delegate: const SlidableDrawerDelegate());
            }
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _openAddFilterDialog(context);
        },
        tooltip: 'Add Filter',
        child: const Icon(
          Icons.add,
          size: 26.0,
          color: Colors.white,
        ),
      ),
    );
  }

  Future<void> _openAddFilterDialog(BuildContext context) async {
    final MaterialPageRoute<String> dialog = MaterialPageRoute<String>(
      builder: (BuildContext context) {
        return const RssFilterDialog();
      },
      fullscreenDialog: true,
    );

    await Navigator.of(context).push(dialog);

    dialog.completed.then((String value) {
      print(value.toString());
      if (value != null) {
        debugPrint('rss url $value');
        _addRssFilter(value);
      }
    });
  }

  void _addRssFilter(String keywords) {
    widget.bloc.filter.add(FilterModel(keywords: keywords.toLowerCase()));
  }
}
