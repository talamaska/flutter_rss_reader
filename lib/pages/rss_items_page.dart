import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/models/feed_item_model.dart';
import 'package:flutter_rss_reader/widgets/custom_progress_indicator.dart';
import 'package:flutter_rss_reader/widgets/error_message.dart';
import 'package:flutter_rss_reader/widgets/no_data.dart';

import 'package:flutter_rss_reader/widgets/rss_list_items.dart';

class RssItemsPage extends StatefulWidget {
  const RssItemsPage({
    this.bloc
  });

  final FeedsBloc bloc;

  @override
  _RssItemsPageState createState() => _RssItemsPageState();
}

class _RssItemsPageState extends State<RssItemsPage> {
  @override
  void initState() {
    super.initState();
    widget.bloc.getFeedItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('News'),
        ),
        body: RefreshIndicator(
          onRefresh: () {
            return widget.bloc.getFeedItems();
          },
          color: Colors.orange,
          child: StreamBuilder<List<FeedItemModel>>(
              stream: widget.bloc.feedsItems,
              initialData: const <FeedItemModel>[],
              builder: (BuildContext context, AsyncSnapshot<List<FeedItemModel>> snapshot) {
                if (snapshot.hasError) {
                  return ErrorMsg(msg: snapshot.error);
                }
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return CustomProgressIndicator();
                  default:
                    if (snapshot.data.isEmpty) {
                      return const NoData(msg: 'No news');
                    }
                    return RssListItems(data: snapshot.data, bloc: widget.bloc);
                }
              }
          ),
        ),
    );
  }
}
