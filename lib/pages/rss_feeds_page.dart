import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/models/feed_model.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/widgets/custom_progress_indicator.dart';
import 'package:flutter_rss_reader/widgets/error_message.dart';
import 'package:flutter_rss_reader/widgets/no_data.dart';
import 'package:flutter_rss_reader/widgets/rss_feed_dialog.dart';
import 'package:flutter_rss_reader/widgets/rss_list.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class RssFeedsPage extends StatefulWidget {
  const RssFeedsPage({this.bloc});
  final FeedsBloc bloc;

  @override
  _RssFeedsPageState createState() => _RssFeedsPageState();
}

class _RssFeedsPageState extends State<RssFeedsPage> {
  StreamSubscription<String> errorSubscription;
  SlidableController slidableController;

  @override
  void initState() {
    widget.bloc.getFeeds();
    slidableController = SlidableController();
    super.initState();
  }

  void _showSnackBar(BuildContext context, String text) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(text),
      duration: Duration(milliseconds: 3000),
    ));
  }

  Future<void> _onDismiss(int index, int id, BuildContext context) async {
    try {
      await widget.bloc.removeFeed(id);
      _showSnackBar(context, 'Feed deleted');
    } catch (e) {
      _showSnackBar(context, 'Feed not deleted');
    }
  }

  void _addRssFeed(String url) {
    widget.bloc.feed.add(FeedModel(url: url.toLowerCase()));
  }

  void _onError(String err, BuildContext context) {
    debugPrint('onError $err');
    showDialog<dynamic>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: const Text('Error'),
          content: Text(err),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Text('Close', style: TextStyle(color: Theme.of(context).accentColor)),
              onPressed: () {
                Navigator.of(context).pop();
                widget.bloc.errorsIn.add(null);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {

    if(errorSubscription != null) {
      errorSubscription.cancel();
    }

    errorSubscription = widget.bloc.errors.listen((String err) {
      if(err != null) {
        _onError(err, context);
      }
    });

    return Scaffold(
      appBar: AppBar(
        title: const Text('Feeds'),
      ),
      body: StreamBuilder<List<FeedModel>>(
          stream: widget.bloc.feeds,
          initialData: const <FeedModel>[],
          builder: (BuildContext context, AsyncSnapshot<List<FeedModel>> snapshot) {
            debugPrint('snapshot ${snapshot.connectionState}');
            if (snapshot.hasError) {
              return ErrorMsg(msg: snapshot.error);
            }
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                  return CustomProgressIndicator();
              default:
                if (snapshot.data.isEmpty) {
                  return const NoData(msg: 'No feeds were saved');
                }
                return RssList(
                  data: snapshot.data,
                  controller: slidableController,
                  onDismiss: _onDismiss,
                  delegate: const SlidableDrawerDelegate(),
                  bloc: widget.bloc
                );
            }
          }
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _openAddFeedDialog(context);
        },
        tooltip: 'Add RSS',
        child: const Icon(Icons.add, size: 26.0, color: Colors.white,),
      ),
    );
  }

  Future<void> _openAddFeedDialog(BuildContext context) async {
    final MaterialPageRoute<String> dialog = MaterialPageRoute<String>(
      builder: (BuildContext context) {
        return const RssFeedDialog();
      },
      fullscreenDialog: true,
    );

    await Navigator.of(context).push(dialog);

    dialog.completed.then((String value) {
      print(value.toString());
      if (value != null) {
        debugPrint('rss url $value');
        _addRssFeed(value);
      }
    });
  }

  @override
  void dispose() {
    errorSubscription.cancel();
    super.dispose();
  }

}


