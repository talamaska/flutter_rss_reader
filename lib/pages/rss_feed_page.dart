import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/models/feed_item_model.dart';
import 'package:flutter_rss_reader/widgets/error_message.dart';
import 'package:flutter_rss_reader/widgets/no_data.dart';
import 'package:flutter_rss_reader/widgets/rss_list_items.dart';

class FeedPage extends StatefulWidget {
  const FeedPage({
    this.bloc,
    this.url,
    this.title
  });

  final FeedsBloc bloc;
  final String url;
  final String title;


  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {

  @override
  void initState() {
    if(widget.url != null) {
      widget.bloc.getFeed(widget.url);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: RefreshIndicator(
        onRefresh: () {
          return widget.bloc.getFeed(widget.url);
        },
        color: Colors.orange,
        child: StreamBuilder<List<FeedItemModel>>(
            stream: widget.bloc.feedItems,
            initialData: const <FeedItemModel>[],
            builder: (BuildContext context, AsyncSnapshot<List<FeedItemModel>> snapshot) {
              if (snapshot.hasError) {
                return ErrorMsg(msg: 'Error: ${snapshot.error}');
              }
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                default:
                  if (snapshot.data.isEmpty) {
                    return const NoData(msg: 'No found news for that feed');
                  }
                  return RssListItems(data: snapshot.data, bloc: widget.bloc);
              }
            }
        ),
      ),
    );
  }
}
