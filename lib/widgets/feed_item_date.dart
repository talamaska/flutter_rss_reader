import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class FeedItemDate extends StatelessWidget {
  const FeedItemDate({
    this.date
  });

  final DateTime date;

  @override
  Widget build(BuildContext context) {
    if(date != null) {
      final String formattedDate = DateFormat.yMMMMd('bg').format(date);
      return Text(
          formattedDate,
          style: Theme.of(context).textTheme.caption.copyWith(
            fontSize: 16.0
          )
      );
    } else {
      return const Text('');
    }
  }

}