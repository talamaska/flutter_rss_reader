import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/models/feed_model.dart';
import 'package:flutter_rss_reader/pages/rss_feed_page.dart';

class RssFeedItem extends StatelessWidget {
  const RssFeedItem({
    this.item,
    this.bloc
  });

  final FeedModel item;
  final FeedsBloc bloc;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.only(left: 8.0, right: 8.0),
      title: Text(
        item.title,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Text(
        item.url,
        overflow: TextOverflow.ellipsis,
      ),
      onTap: () {

        Navigator.push<dynamic>(
          context,
          MaterialPageRoute<dynamic>(builder: (BuildContext context) => FeedPage(
              bloc: bloc,
              url: item.url,
              title: item.title
          )),
        );
      },
    );
  }
}
