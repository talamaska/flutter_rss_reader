import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/models/filter_model.dart';
import 'package:flutter_rss_reader/widgets/filter_list_item.dart';

class FilterList extends StatelessWidget {
  const FilterList({
    this.bloc,
    this.data,
    this.controller,
    this.onDismiss,
    this.delegate
  });

  final List<FilterModel> data;
  final FeedsBloc bloc;
  final SlidableController controller;
  final Function onDismiss;
  final SlidableDelegate delegate;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.vertical,
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return Slidable(
          key: Key(data[index].id.toString()),
          controller: controller,
          direction: Axis.horizontal,
          delegate: delegate,
          actionExtentRatio: 0.25,
          secondaryActions: <Widget>[
            IconSlideAction(
              caption: 'Delete',
              color: Theme.of(context).errorColor,
              icon: Icons.delete,
              onTap: () {
                onDismiss(index, data[index].id, context);
              },
            ),
          ],
          child: FilterListItem(
            bloc: bloc,
            item: data[index]
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) => Divider(
        height: 1.0,
        color: Theme.of(context).dividerColor,
      ),
    );
  }




}
