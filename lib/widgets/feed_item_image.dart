import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/models/feed_item_model.dart';


class FeedItemImage extends StatelessWidget {
  const FeedItemImage({
    this.item,
    this.bloc,
    this.margin
  });

  final FeedItemModel item;
  final FeedsBloc bloc;
  final EdgeInsets margin;

  @override
  Widget build(BuildContext context) {

    return Container(
      width: 50.0,
      height: 50.0,
      margin: margin,
      child: FutureBuilder<String>(
        future: bloc.getItemImage(item), // a previously-obtained Future<String> or null
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          if (snapshot.hasError) {
            return Container(
              color: Colors.grey,
              child: const Icon(Icons.image, color: Colors.white, size: 24.0,),
            );
          }
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Material(
                  type: MaterialType.button,
                  borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                  color: Colors.white,
                  elevation: 2.0,
                  child: const Padding(
                    padding: EdgeInsets.all(16.0),
                    child: CircularProgressIndicator(
                      strokeWidth: 2.5,
                    ),
                  )
              );
            default:
              if (snapshot.data == null) {
                return Material(
                    type: MaterialType.button,
                    borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                    color: Colors.grey,
                    elevation: 2.0,
                    child: Center(child: const Icon(Icons.image, color: Colors.white, size: 24.0,))
                );
              }
              return Stack(
                children: <Widget>[
                  Material(
                    type: MaterialType.button,
                    borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                    color: Colors.grey,
                    elevation: 2.0,
                    child: Center(child: const Icon(Icons.image, color: Colors.white, size: 24.0,))
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(5.0)),
                      color: Colors.white,
                      image: DecorationImage(
                        image: NetworkImage(
                          snapshot.data,
                        ),
                        fit: BoxFit.cover,
                        alignment: Alignment.topCenter,
                      )
                    ),
                  ),
                ]
              );
          }
        },
      ),
    );
  }

}