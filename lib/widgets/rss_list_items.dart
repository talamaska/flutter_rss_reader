import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/models/feed_item_model.dart';

import 'package:url_launcher/url_launcher.dart';

import 'package:flutter_rss_reader/widgets/rss_item.dart';

class RssListItems extends StatelessWidget {
  const RssListItems({
    this.bloc,
    this.data
  });

  final List<FeedItemModel> data;
  final FeedsBloc bloc;

  Future<void> _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }


  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.vertical,
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          onTap: () {
            if(data[index].link != null) {
              _launchURL(data[index].link);
            }
          },
          child: RssListItem(
            item: data[index],
            bloc: bloc
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) => Divider(
        height: 1.0,
        color: Theme.of(context).dividerColor,
      ),
    );
  }
}
