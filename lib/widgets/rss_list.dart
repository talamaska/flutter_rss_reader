import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/models/feed_model.dart';
import 'package:flutter_rss_reader/widgets/rss_feed_item.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class RssList extends StatelessWidget {
  const RssList({
    this.data,
    this.controller,
    this.onDismiss,
    this.delegate,
    this.bloc
  });


  final List<FeedModel> data;
  final SlidableController controller;
  final Function onDismiss;
  final SlidableDelegate delegate;
  final FeedsBloc bloc;


  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.vertical,
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return Slidable(
          key: Key(data[index].id.toString()),
          controller: controller,
          direction: Axis.horizontal,
          delegate: delegate,
          actionExtentRatio: 0.25,
          secondaryActions: <Widget>[
            IconSlideAction(
              caption: 'Delete',
              color: Colors.red,
              icon: Icons.delete,
              onTap: () {
                onDismiss(index, data[index].id, context);
              },
            ),
          ],
          child: RssFeedItem(
            item: data[index],
            bloc: bloc,
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) => Divider(
        height: 1.0,
        color: Theme.of(context).dividerColor,
      ),
    );
  }
}




