import 'package:flutter/material.dart';

class CustomProgressIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        alignment: Alignment.center,
        width: 44.0,
        height: 44.0,
        child: const Material(
            type: MaterialType.circle,
            color: Colors.white,
            elevation: 2.0,
            child: Padding(
              padding: EdgeInsets.all(12.0),
              child: CircularProgressIndicator(
                strokeWidth: 2.5,
              ),
            )
        ),
      ),
    );
  }
}


