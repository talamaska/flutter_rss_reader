import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/models/filter_model.dart';
import 'package:flutter_rss_reader/pages/rss_items_filtered_page.dart';


class FilterListItem extends StatelessWidget {
  const FilterListItem({
    this.bloc,
    this.item
  });

  final FilterModel item;
  final FeedsBloc bloc;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: const EdgeInsets.only(left: 8.0, right: 8.0),
      title: Text(item.keywords),
      dense: true,
      onTap: () {
        Navigator.push<dynamic>(
          context,
          MaterialPageRoute<dynamic>(builder: (BuildContext context) => RssItemsFilteredPage(
            bloc: bloc,
            keywords: item.keywords
          )),
        );
      },
    );
  }
}
