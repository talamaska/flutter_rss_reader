import 'package:flutter/material.dart';

class NoData extends StatelessWidget {

  const NoData({
    @required this.msg
  }) : assert(msg != null);

  final String msg;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Icon(
                Icons.sentiment_dissatisfied,
                color: Theme.of(context).accentColor,
                size: 44.0,
              ),
            ),
            Text(msg, style: TextStyle(color: Theme.of(context).accentColor),),
          ],
        ),
      ),
    );
  }

}