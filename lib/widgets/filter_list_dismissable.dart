import 'package:flutter/material.dart';

import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/models/filter_model.dart';
import 'package:flutter_rss_reader/widgets/filter_list_item.dart';

class FilterListDismissable extends StatelessWidget {
  const FilterListDismissable({
    this.bloc,
    this.data

  });

  final List<FilterModel> data;

  final FeedsBloc bloc;


  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.vertical,
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return Dismissible(
          key: Key(data[index].toString()),
          direction: DismissDirection.endToStart,
          background: Container(
            color: Colors.red,
          ),
          onDismissed: (DismissDirection direction) {
            bloc.removeFilter(data[index].id).then((FilterModel filter) {
              data.removeAt(index);
              Scaffold.of(context).showSnackBar(const SnackBar(
                content: Text('RSS Filter removed'),
              ));
            });

          },
          child: FilterListItem(
              bloc: bloc,
              item: data[index]
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) => Divider(
        height: 1.0,
        color: Theme.of(context).dividerColor,
      ),
    );
  }
}
