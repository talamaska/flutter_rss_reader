import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/models/feed_item_model.dart';
import 'package:flutter_rss_reader/widgets/feed_item_date.dart';
import 'package:flutter_rss_reader/widgets/feed_item_image.dart';
//import 'package:intl/intl.dart';


class RssListItem extends StatelessWidget {
  const RssListItem({
    this.item,
    this.bloc
  });

  final FeedItemModel item;
  final FeedsBloc bloc;

//  Widget _getImage(FeedItemModel item) {
//
//      if(item.image != '' && item.image != null) {
//
//        return Container(
//          decoration: BoxDecoration(
//            borderRadius: const BorderRadius.all(Radius.circular(5.0)),
//            image: DecorationImage(
//              image: NetworkImage(
//                item.image,
//              ),
//              fit: BoxFit.cover,
//              alignment: Alignment.topCenter,
//            )
//          ),
//        );
//
//      } else {
//        return Container(
//          color: Colors.grey,
//        );
//      }
//  }


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[


            FeedItemImage(
              item: item,
              bloc: bloc,
              margin: const EdgeInsets.only(right: 8.0),
            ),


          Container(
            width: MediaQuery.of(context).size.width - 90,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                      item.title,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.subtitle.copyWith(
                        fontSize: 16.0
                      )
                  ),
                  FeedItemDate(date: item.dateObj)
                ]
            ),
          ),
        ],
      ),
    );
  }
}
