import 'package:flutter/material.dart';

class RssFilterDialog extends StatefulWidget {
  const RssFilterDialog();

  @override
  _RssFilterDialogState createState() => _RssFilterDialogState();
}

class _RssFilterDialogState extends State<RssFilterDialog> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController  textController = TextEditingController();

  void onChange(){
    _formKey.currentState.validate();
  }

  @override
  void dispose() {
    super.dispose();
    textController.dispose();
  }

  void _submitForm(String value) {
    if (_formKey.currentState.validate()) {
      print('form is valid');
      Navigator
          .of(context)
          .pop(textController.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add RSS Feed'),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                TextFormField(
                  textInputAction: TextInputAction.go,
                  onFieldSubmitted: _submitForm,
                  controller: textController,
                  autovalidate: false,
                  autofocus: true,
                  maxLines: 1,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'A url is required';
                    }
                  },
                  decoration: const InputDecoration(
                    labelText: 'Keywords',
                    border: UnderlineInputBorder(),
                    hintText: 'comma separated',
                  ),
                ),

                RaisedButton(
                  child: const Text('Submit'),
                  color: Theme.of(context).primaryColor,
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      print('form is valid ${textController.text}');
                      Navigator
                          .of(context)
                          .pop(textController.text);
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}

