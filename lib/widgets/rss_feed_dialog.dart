import 'package:flutter/material.dart';
import 'package:validators/validators.dart';

class RssFeedDialog extends StatefulWidget {
  const RssFeedDialog();

  @override
  _RssFeedDialogState createState() => _RssFeedDialogState();
}

class _RssFeedDialogState extends State<RssFeedDialog> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController  textController = TextEditingController();

  void onChange(){
    _formKey.currentState.validate();
  }

  @override
  void dispose() {
    super.dispose();
    textController.dispose();
  }

  void _submitForm(String value) {
    if (_formKey.currentState.validate()) {
      print('form is valid');
      Navigator
          .of(context)
          .pop(textController.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add RSS Feed'),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                TextFormField(
                  textInputAction: TextInputAction.go,
                  onFieldSubmitted: _submitForm,
                  controller: textController,
                  autovalidate: false,
                  autofocus: true,
                  maxLines: 1,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'A url is required';
                    }
                    final Map<String, dynamic> options = <String, dynamic>{
                      'require_protocol': true,
                      'allow_underscores': true
                    };
                    if(!isURL(value, options)) {
                      return 'Please enter a valid url';
                    }
                  },
                  decoration: const InputDecoration(
                    labelText: 'RSS url',
                    border: UnderlineInputBorder(),
                  ),

                ),

                RaisedButton(
                  child: const Text('Submit'),
                  color: Theme.of(context).primaryColor,
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      print('form is valid ${textController.text}');
                      Navigator
                          .of(context)
                          .pop(textController.text);
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}

