import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/pages/rss_filter_page.dart';
import 'package:flutter_rss_reader/providers/bloc_provider.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/pages/rss_feeds_page.dart';
import 'package:flutter_rss_reader/pages/rss_items_page.dart';
import 'package:flutter_rss_reader/rss_feed_icons.dart';


class App extends StatefulWidget{
  const App({
    Key key
  }) : super(key: key);


  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  FeedsBloc feedsBloc;

  int _currentPage = 0;

  void onTabTapped(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    feedsBloc = BlocProvider.of<FeedsBloc>(context);
    final List<Widget> _children = <Widget>[
      RssFeedsPage(
        bloc: feedsBloc
      ),
      RssItemsPage(
        bloc: feedsBloc
      ),
      RssFilterPage(
        bloc: feedsBloc
      )
    ];

    return Scaffold(
        body: _children[_currentPage],
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTabTapped,
          currentIndex: _currentPage,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home, semanticLabel: 'Feeds',),
              title: Text('Feeds')
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.rss_feed, semanticLabel: 'News',),
              title: Text('News'),
            ),
            BottomNavigationBarItem(
                icon: Icon(RssFeedIcons.filter, semanticLabel: 'Filters',),
                title: Text('Filters')
            ),
          ]
        )
    );
  }

  @override
  void dispose() {
    feedsBloc.dispose();
    super.dispose();
  }

}

