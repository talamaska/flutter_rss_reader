import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter_rss_reader/models/feed_item_model.dart';
import 'package:xml2json/xml2json.dart';
import 'package:http/http.dart' show Client, Response;
import 'package:html/parser.dart' show parse;
import 'package:html/dom.dart';

import 'package:webfeed/webfeed.dart';
import 'package:flutter_rss_reader/models/feed_model.dart';


class FeedProvider {
  Client client = Client();

  Future<FeedModel> getFeed(String url) async {
    try {
      final Response response = await client.get(url);

      if (response.statusCode == 200) {
        final String body = utf8.decode(response.bodyBytes);

        final Xml2Json myTransformer = Xml2Json();
        myTransformer.parse(body);
        final String jsonStr = myTransformer.toBadgerfish();

        try {
          final dynamic json = jsonDecode(jsonStr);


          if (json['feed'] != null) {
            final AtomFeed rssFeed = AtomFeed.parse(body);
            return FeedModel(
                url: url, type: 'atom', title: rssFeed.title ?? 'N/A'
            );
          } else {
            final RssFeed rssFeed = RssFeed.parse(body);
            return FeedModel(
                url: url, type: 'rss', title: rssFeed.title ?? 'N/A'
            );
          }
        } catch (e) {
          final RssFeed rssFeed = RssFeed.parse(body);
          return FeedModel(
              url: url, type: 'rss', title: rssFeed.title ?? 'N/A'
          );
        }
      } else {
        return Future<FeedModel>.error('You have entered invalid rss url');
      }
    } on SocketException {
      return Future<FeedModel>.error('No internet connection or invalid url');
    } on ArgumentError {
      return Future<FeedModel>.error('The url does not respond with rss');
    } on Xml2JsonException {
      return Future<FeedModel>.error('The url does not respond with xml');
    } catch(e) {
      return Future<FeedModel>.error('Something went wrong');
    }
  }

  Future<String> getItemImage(String url) async {
    final Response response = await client.get(url);
    final Document document = parse(response.body);
    final List<Element> metas = document.head.getElementsByTagName('meta');
    final Element imageMeta = metas.firstWhere((Element meta) {
      return meta.attributes.containsValue('og:image');
    });
    final MapEntry<dynamic, String> entry = imageMeta.attributes.entries.firstWhere((MapEntry<dynamic, String> entry) {
      return entry.key == 'content';
    });
    return entry.value;
  }
  

  Future<List<FeedItemModel>> getFeedItems(String url) async {
    final Response response = await client.get(url);

    if(response.statusCode == 200) {
      try {
        final String body = utf8.decode(response.bodyBytes);
        final Xml2Json myTransformer = Xml2Json();
        myTransformer.parse(body);
        final String jsonStr = myTransformer.toBadgerfish();
        try {
          final dynamic json = jsonDecode(jsonStr);


          if (json['feed'] != null) {
            final AtomFeed rssFeed = AtomFeed.parse(body);
            return rssFeed.items.map<FeedItemModel>((AtomItem item) {
              String image;
              if(item.links.isNotEmpty) {
                final AtomLink imageTag = item.links.firstWhere((AtomLink link) {
                  return link.type.contains('image');
                });
                if(imageTag != null) {
                  image = imageTag.href;
                }
              }

              return FeedItemModel(
                  title: item.title,
                  date: item.updated,
                  link: null,
                  image: image,
                  description: null
              );
            }).toList();
          } else {
            final RssFeed rssFeed = RssFeed.parse(body);
            return rssFeed.items.map<FeedItemModel>((RssItem item){

              final FeedItemModel feedItem =  FeedItemModel(
                  title: item.title,
                  link: item.link,
                  description: item.description != null ? item.description : null,
                  date: item.pubDate != null ? item.pubDate : (item.dc != null && item.dc.date != null ? item.dc.date : ''),
                  image: item.enclosure != null && item.enclosure.url != null ? item.enclosure.url : null
              );

              return feedItem;
            }).toList();
          }
        } catch (e) {
          final RssFeed rssFeed = RssFeed.parse(body);
          return rssFeed.items.map<FeedItemModel>((RssItem item){

            final FeedItemModel feedItem =  FeedItemModel(
                title: item.title,
                link: item.link,
                description: item.description != null ? item.description : null,
                date: item.pubDate != null ? item.pubDate : (item.dc != null && item.dc.date != null ? item.dc.date : ''),
                image: item.enclosure != null && item.enclosure.url != null ? item.enclosure.url : null
            );

            return feedItem;
          }).toList();
        }



      } catch(e) {
        return Future<List<FeedItemModel>>.error('The url does not respond with rss 2');
      }
    } else {
      return Future<List<FeedItemModel>>.error('You have entered invalid rss url 2');
    }

  }


}