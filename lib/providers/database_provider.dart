import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter_rss_reader/daos/feed_dao.dart';
import 'package:flutter_rss_reader/daos/filter_dao.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  factory DatabaseProvider() {
    return _instance;
  }
  DatabaseProvider._internal();

  static final DatabaseProvider _instance = DatabaseProvider._internal();
  static DatabaseProvider get = _instance;
  bool isInitialized = false;
  Database _db;



  Future<Database> db() async {
    debugPrint('db() $isInitialized');
    if(!isInitialized) {
      debugPrint('init');
      await _init();
      isInitialized = true;
      debugPrint('Initialized');
    }

    return _db;
  }

  Future<Database> _init() async {
    final String databasesPath = await getDatabasesPath();
    final String path = join(databasesPath, 'rss.db');
    Sqflite.setDebugModeOn();
    _db = await openDatabase(
        path,
        version: 1,
        onCreate: (Database db, int version) async {
          await db.execute(FeedDao().createTableQuery);
          await db.execute(FilterDao().createTableQuery);
        }
    );

    return _db;
  }
}