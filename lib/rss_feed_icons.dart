import 'package:flutter/widgets.dart';

class RssFeedIcons {
  RssFeedIcons._();

  static const String _kFontFam = 'RssFeedIcons';

  static const IconData filter = IconData(0xf0b0, fontFamily: _kFontFam);
}