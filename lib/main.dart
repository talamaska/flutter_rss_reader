import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rss_reader/providers/bloc_provider.dart';
import 'package:flutter_rss_reader/providers/database_provider.dart';
import 'package:flutter_rss_reader/blocs/feeds_bloc.dart';
import 'package:flutter_rss_reader/app.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final DatabaseProvider db = DatabaseProvider();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: GlobalMaterialLocalizations.delegates,
      supportedLocales: const <Locale>[
        Locale('en', 'US'), // American English
        Locale('en', 'UK'), // British English
        Locale('fr', 'FR'), // French
        Locale('bg')
      ],
      title: 'RSS Reader',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: BlocProvider<FeedsBloc>(
        bloc: FeedsBloc(db),
        child: const App(),
      ),
    );
  }
}


